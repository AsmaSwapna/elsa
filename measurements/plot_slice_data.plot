set terminal pdfcairo
set output 'results/slice-create.pdf'
set grid
set yrange[0:25000] 
set key font ",8"
set key spacing 0.75
set title 'Slice Resources Allocation'
set xlabel 'number of resources (servers)'
set ylabel 'time (ms)'
plot 'results/slice-create.dat' u 1:2:6 w yerrorlines t 'Computational Resources', 'results/slice-create.dat' u 1:3:7 w yerrorlines t 'Network Resources', 'results/slice-create.dat' u 1:4:8 w yerrorlines t 'Resources Abstraction', 'results/slice-create.dat' u 1:5:9 w yerrorlines t 'Resources Monitoring', 'results/slice-create.dat' u 1:($2+$3+$4+$5) w lp t 'Total Time'

set output 'results/slice-delete.pdf'
set grid
set yrange[0:11000]
set title 'Slice Resources Deallocation'
set xlabel 'number of resources (servers)'
plot 'results/slice-delete.dat' u 1:2:6 w yerrorlines t 'Computational Resources', 'results/slice-delete.dat' u 1:3:7 w yerrorlines t 'Network Resources', 'results/slice-delete.dat' u 1:4:8 w yerrorlines t 'Resources Abstraction', 'results/slice-delete.dat' u 1:5:9 w yerrorlines t 'Resources Monitoring', 'results/slice-delete.dat' u 1:($2+$3+$4+$5) w lp t 'Total Time'

set terminal pdfcairo
set output 'results/slice-create_secs.pdf'
set grid
set yrange[0:25]
set ytics 2
set key font ",8"
set key spacing 0.75
set title 'Slice Resources Allocation'
set xlabel 'number of resources (servers)'
set ylabel 'time (s)'
plot 'results/slice-create.dat' u 1:($2/1000):($6/1000) w yerrorlines t 'Computational Resources', 'results/slice-create.dat' u 1:($3/1000):($7/1000) w yerrorlines t 'Network Resources', 'results/slice-create.dat' u 1:($4/1000):($8/1000) w yerrorlines t 'Resources Abstraction', 'results/slice-create.dat' u 1:($5/1000):($9/1000) w yerrorlines t 'Resources Monitoring', 'results/slice-create.dat' u 1:($2+$3+$4+$5)/1000 w lp t 'Total Time'
