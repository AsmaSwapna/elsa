import os, json
import statistics

class ProcessData:
      def __init__(self, base_path='measurements'):
          self.base_path = base_path
          self.__get_measurements_dirs()


      def __get_measurements_dirs(self):
          filenames = os.listdir(self.base_path)
          self.measurements_dirs = []
          for filename in filenames:
              if os.path.isdir(os.path.join(os.path.abspath(self.base_path), filename)):
                 if str.isdigit(filename):
                    self.measurements_dirs.append(filename)
          self.measurements_dirs.sort()


      def __get_slice_measurement(self, exp, operation):
          filenames = os.listdir(exp)
          measurements = {'parts':[], 'connections':[], 'abstractions':[], 'monitoring':[]}
          for filename in filenames:
              if 'slice-' + operation in filename:
                 f = open(exp + '/' + filename, 'r')
                 current_measurement = json.load(f)
                 measurements['parts'].append(current_measurement['parts'])
                 measurements['connections'].append(current_measurement['connections'])
                 measurements['abstractions'].append(current_measurement['abstractions'])
                 measurements['monitoring'].append(current_measurement['monitoring'])

          return measurements


 
      def compute_slice_timings(self, operation):
          f_name = 'results/slice-' + operation + '.dat'
          f = open(f_name, 'w')
          for exp in self.measurements_dirs:
              measurements = self.__get_slice_measurement(exp, operation)
              avg_parts = statistics.mean(measurements['parts'])
              avg_connections = statistics.mean(measurements['connections'])
              avg_abstractions = statistics.mean(measurements['abstractions'])
              avg_monitoring = statistics.mean(measurements['monitoring'])
              
              conf_int_parts = 1.645 * statistics.pstdev(measurements['parts']) / len(measurements['parts'])
              conf_int_connections = 1.645 * statistics.pstdev(measurements['connections']) / len(measurements['connections'])
              conf_int_abstractions = 1.645 * statistics.pstdev(measurements['abstractions']) / len(measurements['abstractions'])
              conf_int_monitoring = 1.645 * statistics.pstdev(measurements['monitoring']) / len(measurements['monitoring'])

              line = exp + ' ' + str(avg_parts) + ' ' + str(avg_connections) + ' ' + str(avg_abstractions) + ' ' + str(avg_monitoring) + ' ' + \
                     str(conf_int_parts) + ' ' + str(conf_int_connections) + ' ' + str(conf_int_abstractions) + ' ' + str(conf_int_monitoring) + ' ' + '\n'
              f.write(line)
          f.close()


 

      def __get_service_runs(self, exp):
          filenames = os.listdir(exp)
          service_runs = []
          for filename in filenames:
              if 'service-orch-create-' in filename:
                 raw_service_run = filename.split('service-orch-create-')[1] #using the create only as it should be identical to delete
                 service_run = int(raw_service_run.split('-nf')[0])
                 if service_run not in service_runs:
                    service_runs.append(service_run)

          return service_runs



      def __get_service_orch_filenames(self, exp, operation):
          service_runs = self.__get_service_runs(exp)
          orchestration_filenames = dict()
          filenames = os.listdir(exp)
  
          #init lists in dict
          for run in service_runs:
              orchestration_filenames[run] = []

          #populating dict
          for filename in filenames:
              if 'service-orch-' + operation in filename:
                 for run in service_runs:
                     if str(run) + '-' in filename:
                        orchestration_filenames[run].append(filename)
          return orchestration_filenames


      def __compute_service_orch_timing(self, file_p):
          for line in file_p.readlines():
              if 'OVERALL,' in line and 'START' in line:
                 t_start = line.split(',')[4]
              elif 'ORCHESTRATION,' in line and 'END' in line:
                 t_end = line.split(',')[4]
          return round(float(t_end)-float(t_start))
                

          
      def __get_service_orch_timings(self, exp, operation): # might want to pass exp and remove from dict
          stats_dict = self.__get_service_orch_filenames(exp, operation)
          timings_dict = dict()

          for service_run in stats_dict.keys():
              timings_dict[service_run] = []
              for stat_file in stats_dict[service_run]:
                  stat_file_p = open(exp + '/' + stat_file)
                  timings_dict[service_run].append(self.__compute_service_orch_timing(stat_file_p))
                  stat_file_p.close()

          return timings_dict
                

     
      def write_service_orch_stats(self, exp, operation):
          timings_dict = self.__get_service_orch_timings(exp, operation)
          stats_p = open('results/service-orch-' + operation + '-' + exp + '.dat', 'w')
          for service_run in sorted(timings_dict.keys()):
              avg_time = statistics.mean(timings_dict[service_run])
              stdev_time = statistics.pstdev(timings_dict[service_run])
              conf_int = 1.645 * stdev_time / len(timings_dict[service_run])
              line = str(service_run) + ' ' + str(avg_time) + ' ' + str(conf_int) + '\n'
              stats_p.write(line)

          stats_p.close()


      def compute_service_orch_timings(self, operation):
          for exp in self.measurements_dirs:
              self.write_service_orch_stats(exp, operation)


  
      def __get_filename_uuid(self, filename):
          filename_no_ext = filename.split('.')[0]
          ft = filename_no_ext.split('-')
          ftl = len(ft)
          uuid = ft[ftl-6] + '-' + ft[ftl-5] + '-' + ft[ftl-4] + '-' + ft[ftl-3] + '-' + ft[ftl-2]
          return uuid


      def __get_runs_uuids(self, run_list):
          uuids = []
          for filename in run_list:
              uuid = self.__get_filename_uuid(filename)
              if uuid not in uuids:
                 uuids.append(uuid)
          return uuids


      def __get_service_adapt_filenames(self, exp, operation):
          service_runs = self.__get_service_runs(exp) #the result should be identical for orch and adapt
          adaptation_filenames = dict()
          filenames = os.listdir(exp)

          #init lists in dict
          for run in service_runs:
              adaptation_filenames[run] = []

          #populating dict
          for filename in filenames:
              if 'service-adapt-' + operation in filename:
                 for run in service_runs:
                     if str(run) + '-' in filename:
                        adaptation_filenames[run].append(filename)

          #processing dict to identify multiple runs
          proc_adaptation_filenames = dict()
          for run in adaptation_filenames.keys():
              proc_adaptation_filenames[run] = dict()
              run_uuids = self.__get_runs_uuids(adaptation_filenames[run])
              for r_uuid in run_uuids:
                  proc_adaptation_filenames[run][r_uuid] = []
                  
                  for filename in adaptation_filenames[run]:
                      if r_uuid in filename:
                         proc_adaptation_filenames[run][r_uuid].append(filename)

          return proc_adaptation_filenames



      def __compute_service_adapt_timing(self, file_p):
          for line in file_p.readlines():
              if 'adaptation_tot' in line:
                 return int(line.split('=')[1])/1000


      def __get_service_adapt_timings(self, exp, operation):
          stats_dict = self.__get_service_adapt_filenames(exp, operation)
          timings_dict = dict()

          for service_run in stats_dict.keys():
              timings_dict[service_run] = dict()
              for uuid in stats_dict[service_run].keys():
                  run_timings = []
                  for stat_file in stats_dict[service_run][uuid]:
                      stat_file_p = open(exp + '/' + stat_file)
                      run_timings.append(self.__compute_service_adapt_timing(stat_file_p))
                      timings_dict[service_run][uuid] = int(statistics.mean(run_timings))
                      stat_file_p.close()

          return timings_dict


      def write_service_adapt_stats(self, exp, operation):
          timings_dict = self.__get_service_adapt_timings(exp, operation)
          stats_p = open('results/service-adapt-' + operation + '-' + exp + '.dat', 'w')
          for service_run in sorted(timings_dict.keys()):
              avg_time = statistics.mean(timings_dict[service_run].values())
              stdev_time = statistics.pstdev(timings_dict[service_run].values())
              conf_int = 1.645 * stdev_time / len(timings_dict[service_run].values())
              line = str(service_run) + ' ' + str(avg_time) + ' ' + str(conf_int) + '\n'
              stats_p.write(line)

          stats_p.close()


      def compute_service_adapt_timings(self, operation):
          for exp in self.measurements_dirs:
              self.write_service_adapt_stats(exp, operation)






if __name__=='__main__':
   pr = ProcessData('.')

   pr.compute_slice_timings('create')
   pr.compute_slice_timings('delete')
 
   pr.compute_service_orch_timings('create')
   pr.compute_service_orch_timings('delete')

   pr.compute_service_adapt_timings('create')
   pr.compute_service_adapt_timings('delete')






   

