#!/bin/bash

path=$1

for file in `ls ${path}/service-adapt-*`
    do
      num_lines=`cat ${file} | wc -l`
      if [ ${num_lines} -lt 5 ]; then
         echo $file
      fi
    done
