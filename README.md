# ELSA – Experiments with Large-scale lightweight Service Slices
This file provides guidance to install and run the ELSA demo.

Copyright 2019 NECOS Consortium
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file and software referenced except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

A video of the Demo execution can be found [here](https://www.youtube.com/watch?v=96ySW-jEelI).

### Introduction
The fundamental idea of this demonstration is to show the dynamic deployment of end-to-end **Bare-Metal Slices** that can be utilised by a Tenant to instantiate services consisting of a *very large number of lightweight elements* [i.e., Virtual Network Functions  (VNFs) and Virtual Links]. The allocated slices can span across multiple Resources Providers, such as those located at the Edge of the infrastructure which may have limited resource capabilities.

This demonstration is based on both brand new components that were developed to support some of the functionalities of the NECOS architecture (see Section *Demo Components*) and existing open source software and tools that were adapted to support the functionalities of a Lightweight VIM (VLSP), the Service Embedding Engine (ESCAPE) and a service topology generator (Virtualizer Service Generator).

The list of functionalities implemented in this demo is a sub-set of those described in [NECOS](http://www.h2020-necos.eu/) Project [deliverables](http://www.h2020-necos.eu/documents/deliverables/) (D5.2 and D6.2).


### Components of ELSA
The NECOS components that have been specifically developed and used for this demonstration are detailed in the following.

##### Slice Builder
The functionalities of this component required for the demonstration have been implemented in the **slice_provider** module available in this repo. It allows building an end-to-end Slice by triggering the request of Slice Parts (which are specifically based on on-demand instances of the lightweight *VLSP* VIM) to a given number of DC Slice Controllers (see description below) whose entry-points have been specified via a YAML descriptor.

##### Slice Resource Orchestrator 
The functionalities of this NECOS component that have been implemented in the **slice_provider** module are limited to the final stitching of the Slice Parts (i.e., creation of the required network interconnection) as well as to triggering the deployment of the required resource and monitoring abstractions through the IMA.

##### IMA
The IMA functionalities implemented in this demo consist in the full deployment of Monitoring Adapters and Slice Measurements Aggregators via the Slice Monitoring Controller (for what concerns the IMA Monitoring side). The IMA is based on a bespoke adaptation of the Lattice Monitoring Framework available at https://gitlab.com/necos/lattice-monitoring-framework). The resource abstraction is based on specific adapters to the VLSP VIM (their implementation is not available in this repo due to license constraints) instantiated via the **slice_provider**, which have been designed to seamlessly interwork with the ESCAPE Orchestrator deployed in the Tenant domain.

##### DC Slice Controller
This particular implementation of the DC Slice Controller has been developed to provide the on-demand instantiation of DC Slice Parts running the lightweight VLSP VIM. It is also based on the creation of DC Slice Parts formed of physical servers and as such supports the NECOS feature of bare-metal slice. Current code available at https://gitlab.com/necos/ucl-dc-slice-controller.

##### Tenant
The functionalities of the Tenant have been implemented via the **tenant** module available in this repo. It allows triggering the instantiation of end-to-end Slices via YAML descriptors (available under **tenant**) and to submit large-scale service requests to be deployed on the Slices via the *ESCAPE* orchestrator. The service descriptors are also available in the same directory.

### Demo external components
Existing Open Source components that have been utilised with some integrations / adjustments are reported below.

##### ESCAPE
This is an open source project that was developed in the context of the EU projects Unify and 5GEx. It is utilised in this demonstration to perform the embedding of service elements on the created slices. Original source code is available at https://github.com/hsnlab/escape.

##### Virtualizer Service Generator
This was developed in the context of the EU 5GEx project and allowed the generation of service topologies of different sizes (from hundreds to a couple of thousands of elements) to be submitted to the ESCAPE orchestrator during this demonstration. Original source code is available at https://github.com/5GExchange/virtualizer-service-generator.

##### VLSP
This is a platform that provides the functionalities of a lightweight VIM and allows the deployment of service elements in the form of interconnected virtual routers and links. Current source code is available at https://github.com/stuartclayman/VLSP.

### Repository Structure Overview
* **/scripts**: this is the folder containing the scripts that can be used to start and stop all the required demo components (once customised with the specific environmental settings).
* **/tenant**: contains the source code that implements the tenant domain and the related REST interface.
* **/slice_provider**: contains the source code related to the implementation of the Service Provider components (Slice Builder and Slice Resource Orchestrator).
* **/resource_provider**: this folder is a placeholder for the external software projects that implements the components of the Resource Provider (i.e., the VLSP VIM and the DC Slice Controller).
* **/measurements**: contains the scripts and service descriptors that have been utilised to gather data related to the execution of the demo, as well as to generate the graphs reported in the deliverables D5.2 and D6.2.

### How to install ELSA
This section describes the steps required for the installation, configuration and initilisation of the main demo components. Please note that this can be automated via the scripts available under the directory *scripts* on this repo (please customise the scripts with your installation paths and use ```setup_env.sh``` to start the components).

Ideally this demo requires an infrastructure consisting of 3 separate servers, each running the software related to the main sub-systems, i.e., the Tenant, the Slice Provider and the Reosurce Provider. Additional hosts can be added to add more resources to the Resource Provider or increase the total number of Resource Providers. This allows the deployment of end-to-end Slices that consists of a greater number of Slice Parts.

##### Resource Provider
###### Slice Controller
Download the *DC Slice Controller* software component (from https://gitlab.com/necos/ucl-dc-slice-controller), compile the source code using the provided ant script and run:
```sh
java slice.SliceController config.json
```
*config.json* is a file describing the topology of the physical resources (see included examples in the repo).

The DC Slice Controller currently supports the *VLSP* Lightweight VIM, which can be download from https://github.com/stuartclayman/VLSP. VLSP needs to be compiled using the provided ant script. The path of the compiled VLSP classes has to be specified in the DC Slice Controller configuration file.


##### Slice Provider
###### Orchestration Components
Download the *slice_provider* software from this repo and start the component using:
```sh
python http_api.py
```
###### IMA Controller
Download the *IMA* software component (from https://gitlab.com/necos/lattice-monitoring-framework), compile it using the provided ant script and run:
```sh
java -cp monitoring-bin-controller-2.0.1.jar mon.lattice.control.controller.json.ZMQController
```


##### Tenant
###### Tenant domain and interface
Download the *tenant* software from this repo and start the component using:
```sh
python tenant_api.py
```

###### ESCAPE Orchestrator
Download the *ESCAPE* Orchestrator from https://github.com/hsnlab/escape and create a new container using the provided instructions (please note that Docker is required for that).



### Running ELSA
Customise the ```slice_activator.py``` file provided under */tenant* in order to submit the creation of a new slice to the tenant REST interface. Please specify the path to the descriptors as in the example below (current slice descriptors are available under ```tenant/slice_descriptors```). The same python file can be used to request the instantiation of the service instances on the slices (using the service descriptors under ```tenant/service_descriptors```, e.g.:

```python
slice1 = SliceExperiment('05', 'localhost', 5001, 'slice_descriptors/slice_info_5_a.yml')
slice1.create_slice()
slice1.submit_service('90-nf', 'service_descriptors/edit_config_create_1-90_1567177197932.xml')
slice1.delete_service('90-nf', 'service_descriptors/edit_config_delete_1-90_1567177197932.xml') 
```

Alternatively the request can be submitted to the REST API directly via e.g., the command line using 
```sh
curl --data-binary @<slice_desc>.yml -X POST -H "Content-Type: text/x-yaml" http://<address>:<port>//tenant/slice
``` 

The default port used by the Tenant interface is ```5001```. The call returns the handle to the created slice instances, which can then be used to submit a service request to the ESCAPE orchestrator using the virtualiser descriptor files available in this repo (under ```measurements/current_service_descriptors```).


