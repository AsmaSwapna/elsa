from flask import Flask, jsonify, request, make_response
from tenant_slice import Slice
import time
import traceback

app = Flask(__name__)

slice_orch_host = 'localhost'
slice_orch_port = 5000
slices = dict()

service_orch_port = 8888

#curl --data-binary @slice_info.yml -X POST -H "Content-Type: text/x-yaml" http://localhost:5000/orchestrator/slice
@app.route('/tenant/slice', methods=['POST'])
def create_slice():
    try:
        global service_orch_port
        print 'Requesting slice to the Slice Resource Orchestrator: ' + slice_orch_host + ':' + str(slice_orch_port)
        tenant_slice = Slice(slice_orch_host, slice_orch_port, service_orch_port, request.data)
        service_orch_port = service_orch_port + 1
        tenant_slice.create_slice()
        tenant_slice.get_slice_entrypoint()
        tenant_slice.create_tar()
        tenant_slice.start_service_orchestrator()

        slice_id = tenant_slice.get_id()
        slices[slice_id] = tenant_slice
        print 'Created slice: ' + str(slice_id)
        return jsonify({'slice_id' : slice_id, 'service_orchestrator' : tenant_slice.get_orchestrator_entrypoint(), 'slice_parts': tenant_slice.get_parts_list()})

    except Exception as ex:
        print ex
        traceback.print_exc()
        return make_response(jsonify({'error': 'Internal Server Error'}), 500)


@app.route('/tenant/slice/<string:slice_id>', methods=['DELETE'])
def delete_slice(slice_id):
    try:
        global service_orch_port
        tenant_slice = slices[slice_id]
        tenant_slice.stop_service_orchestrator()
        tenant_slice.delete_slice()
        service_orch_port = service_orch_port - 1
        return jsonify({'result' : True})
    except:
        make_response(jsonify({'error': 'Internal Server Error'}), 500)

        

@app.route('/tenant/slice/', methods=['GET'])
def list_slices():
    try:
        print slices.keys()
        return jsonify({'slices': slices.keys()})
    except:
        return make_response(jsonify({'error': 'Internal Server Error'}), 500)


if __name__ == '__main__':
    app.run(port=5001, debug=True)
