import requests
import docker
import tarfile
import time
from StringIO import StringIO
import json
import os

class Slice:
      def __init__(self, host, port, service_orch_port, slice_info):
          self.orchestrator_host = host
          self.orchestrator_port = str(port)
          self.service_orch_port = service_orch_port
          self.slice_info = slice_info
          self.service_orch_host = 'localhost' # we bind all the containers to localhost on different ports

      @classmethod
      def from_file(cls, slice_info_file):
          slice_info_ref = open(slice_info_file, 'r')
          return cls(slice_info=slice_info_ref)


      def create_slice(self):
          #uses slice defined as yaml
          url = 'http://' + self.orchestrator_host + ':' + self.orchestrator_port + '/orchestrator/slice'
          r = requests.post(url, data=self.slice_info, headers={'Content-Type': 'text/x-yaml'}).json()
          self.slice_id = r['slice_id']
          self.slice_parts = r['slice_parts']
          print self.slice_id


      def get_slice_entrypoint(self):
          url = 'http://' + self.orchestrator_host + ':' + self.orchestrator_port + '/orchestrator/slice/' + self.slice_id
          r = requests.get(url).json()
          self.entry_point = json.dumps(r['entry_point'])


      def create_tar(self):
          s = StringIO(self.entry_point)

          t_info = tarfile.TarInfo('slice.config')
          t_info.mtime = time.time()
          t_info.size = len(s.getvalue())
          tar = tarfile.open('/tmp/slice_data-' + self.slice_id, 'w')
          tar.addfile(t_info, fileobj=s)
          tar.close()

     
      def start_service_orchestrator(self):
          slice_entrypoint = open('/tmp/slice_data-' + self.slice_id)
          client = docker.from_env()
          self.instance_name = 'orch-' + self.slice_id
          client.containers.create('mdo/escape2', name=self.instance_name, ports={9999 : (self.service_orch_host, self.service_orch_port)}, detach=True)
          orchestrator = client.containers.get(self.instance_name)
          orchestrator.put_archive('/opt/escape/config/', slice_entrypoint)
          orchestrator.start()
          self.service_orch_entrypoint = 'http://' + self.service_orch_host + ':' + str(self.service_orch_port) + '/escape/orchestration/'
          #waiting for the http API to come up
          time.sleep(3)
         

      def stop_service_orchestrator(self):
          client = docker.from_env() 
          orchestrator = client.containers.get(self.instance_name)
          orchestrator.stop()
          orchestrator.remove()
          os.remove('/tmp/slice_data-' + self.slice_id) 


      def delete_slice(self):
          url = 'http://' + self.orchestrator_host + ':' + self.orchestrator_port + '/orchestrator/slice/' + self.slice_id
          r = requests.delete(url).json()
          print r['result']


      def get_id(self):
          return self.slice_id


      def get_orchestrator_port(self):
          return self.service_orch_port


      def get_orchestrator_entrypoint(self):
          return self.service_orch_entrypoint


      def get_parts_list(self):
          return self.slice_parts
       

if __name__ == '__main__':
   t = Slice.fromfile('localhost', 5000, 'slice_info.yml')
   t.create_slice()
   t.get_slice_entrypoint()
   t.create_tar()
   t.start_service_orchestrator()
