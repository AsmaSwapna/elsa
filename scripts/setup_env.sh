#!/bin/bash

sudo ./cleanUp-clayone-docker.sh
./cleanUp-clayone.sh
./cleanUp-clay.sh
./cleanUp-gas.sh
./cleanUp-edu.sh

ssh claytwo "screen -dmS slicemng-clay ~/NECOS/exp_scripts/startSliceCtrl-clay.sh"
ssh methane "screen -dmS slicemng-gas ~/NECOS/exp_scripts/startSliceCtrl-gas.sh"
ssh washington "screen -dmS slicemng-edu ~/NECOS/exp_scripts/startSliceCtrl-edu.sh"

./startComponents.sh
./startViewers.sh
./startTunnels.sh
