#!/bin/bash

hosts=(claytwo claythree clayfive)

for host in ${hosts[@]}; do
    ssh $host "killall java"
    ssh $host "rm -f /tmp/*channel*.out && rm -f /tmp/data-source-*.log"
    ssh $host 'if [ ! -z "`ps aux | grep java`" ]; then killall -9 java; fi'
done

ssh claytwo "screen -list | grep do | cut -d '.' -f 1 | xargs kill &>/dev/null"
